from typing import (
    NoReturn,
    Self
)


class ProtocolError(Exception):
    def __init__(
        self: Self,
        message: str
    ) -> NoReturn:
        self.message = message

class SecurityError(ProtocolError):
    ...

class PhoneNumberError(ProtocolError):
    ...

class WrongPasswordError(ProtocolError):
    ...

class ValueExists(ProtocolError):
    ...
