from os import getenv
from rethinkdb import RethinkDB
from rethinkdb.errors import (
    ReqlOpFailedError
)
from typing import (
    NoReturn,
    Self,
    Union
)
from uuid import UUID


class RethinkDBQuery:

    def __init__(
        self: Self,
        table: str
    ) -> NoReturn:
        self.__table_name = table
        self.__server = RethinkDB()
        self.__connection = self.__server.connect()
        self.__server.db_create(
            getenv(
                'WORKERY_NAME'
            )
        ).run(
            self.__connection
        )
        self.__connection = self.__server.connect(
            db=getenv(
                'WORKERY_NAME'
            )
        )
        self.__server.table_create(
            self.__table_name
        ).run(
            self.__connection
        )
        self.__table = self.__server.table(
            self.__table_name
        )

    def insert(
        self: Self,
        data: dict
    ) -> str:
        """
            Insert data, return id.
        """
        return self.__table.insert(
            data
        ).run(
            self.__connection
        )[
            'generated_keys'
        ][0]

    def update(
        self: Self,
        identifier: Union[UUID, str],
        values: dict
    ) -> int:
        return self.__table.get(
            identifier
        ).update(
            values
        ).run(
            self.__connection
        )['replaced']

    def find(
        self: Self,
        value: dict
    ) -> list[dict]:
        return [
            a for a in self.__table.filter(
                value
            ).run(
                self.__connection
            )
        ]
