from secrets import choice
from string import (
    ascii_letters, digits
)
from typing import Union


def randomize(
    size: Union[int, None] = 20
) -> str:
    return ''.join(
        choice(
            ascii_letters + digits
        ) for iterator in range(
            size
        )
    )
