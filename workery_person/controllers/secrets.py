from cryptography.fernet import Fernet
from os import getenv
from typing import (
    NoReturn,
    Self,
    Union
)

class Secret:

    __fernet = Fernet(
        getenv(
            'WORKERY_KEY'
        )
    )

    @classmethod
    def encrypt_text(
        cls: Self,
        text: Union[str, bytes]
    ) -> str:
        return cls.__fernet.encrypt(
            text.encode(
                encoding='utf-8'
            )
        ).decode(
            encoding='utf-8'
        ) if type(
            text
        ) == str else cls.__fernet.encrypt(
            text
        ).decode(
            encoding='utf-8'
        )

    @classmethod
    def decrypt_text(
        cls: Self,
        text: Union[str, bytes]
    ) -> str:
        return cls.__fernet.decrypt(
            text
        ).decode(
            'utf-8'
        )
