from workery_person.controllers.addresses import (
    Address
)
from workery_person.controllers.auths import (
    Auth
)
from workery_person.controllers.persons import (
    Person
)
from workery_person.controllers.roles import (
    Role
)


__all__ = [
    'Address',
    'Auth',
    'Person',
    'Role'
]
