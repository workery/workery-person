from typing import (
    NoReturn,
    Self,
    Union
)
from uuid import UUID
from workery_person.connectors.rethinkdb_queries import (
    RethinkDBQuery
)


class Address:

    __query = RethinkDBQuery(
        table='addresses'
    )

    def __init__(
        self: Self,
        identifier: Union[UUID, str]
    ) -> NoReturn:
        """
            :identifier: The auth table identifier.
        """
        self.identifier = identifier

    def new(
        self: Self,
        location: str,
        district: str,
        city: str,
        state: str,
        country: str
    ) -> str:
        """
            Create new address in database.
            Returns id.
        """
        return self.__query.insert(
            {
                'person': self.identifier,
                'location': location,
                'district': district,
                'city': city,
                'state': state,
                'country': country
            }
        )
    def update(
        self: Self,
        record: Union[UUID, str],
        values: dict
    ) -> int:
        """
            Change address in database.
            Show replaced rows.
        """
        return self.__query.update(
            identifier=record,
            values=values
        )

    @property
    def all(
        self: Self
    ) -> list[dict]:
        """
            Shows all addresses registered \
by the person who was started \
in the class instance.
        """
        return self.__query.find(
            value={
                'person': self.identifier
            }
        )
