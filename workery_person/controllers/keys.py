from os import getenv
from redis import Redis
from typing import (
    NoReturn,
    Self,
    Union
)
from workery_person.exceptions import (
    SecurityError
)
from workery_person.utils import (
    randomize
)


class Key:
    __connection = Redis.from_url(
        getenv(
            'WORKERY_REDIS'
        )
    )
    __defaults = {
        'ex': getenv(
            'WORKERY_REDIS_EPIRE'
        ),
        'nx': True
    }

    @classmethod
    def new(
        cls: Self,
        value: dict
    ) -> str:
        key = randomize()
        cls.__connection.set(
            name=key,
            value=value,
            **cls.__defaults
        )
        return key

    @classmethod
    def recover(
        cls: Self,
        key: str,
        value: str
    ) -> Union[str, None]:
        db_value = cls.__connection.get(
            key
        )
        if db_value == None:
            raise SecurityError(
                f'Key {key} not found.'
            )
        if db_value.decode() == value:
            cls.__connection.delete(key)
            new_key = randomize()
            cls.__connection.set(
                name=new_key,
                value=value,
                **cls.__defaults
            )
            return new_key
        else:
            cls.__connection.delete(
                key
            )
            raise SecurityError(
                f'Error in key {key} or value {value}.'
            )
