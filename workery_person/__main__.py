if __name__ == '__main__': # pragma: no cover
    from faker import Faker
    from os import getenv
    from psycopg import connect
    from psycopg.errors import UniqueViolation
    from rethinkdb import RethinkDB
    from workery_person.connectors.rethinkdb_queries import (
        RethinkDBQuery
    )
    from workery_person.connectors.sql_queries import (
        SQLQuery
    )
    from workery_person.controllers.addresses import (
        Address
    )
    from workery_person.controllers.auths import (
        Auth
    )
    from workery_person.controllers.keys import (
        Key
    )
    from workery_person.controllers.persons import (
        Person
    )
    from workery_person.controllers.roles import (
        Role
    )
    from workery_person.controllers.secrets import (
        Secret
    )
    from workery_person.exceptions import (
        SecurityError,
        ValueExists
    )
    from workery_person.utils import randomize


    faker = Faker(
        locale='pt_br'
    )
    connection = connect(
        getenv(
            'WORKERY_SQL'
        )
    )
    email = 'eumesmo@exemplo.com'
    phone = '+5513992031899'
    password = '123456'
    try:
        Person.auth.new(
            email=email,
            phone=phone,
            password=password
        )
    except ValueExists:
        pass
    me = Person(
        identifier=Person.auth.login(
            value=phone,
            password=password
        )
    )
    me.address.new(
        location='Rua das casas, sn',
        district='Favela do caixão',
        city='Piripiri',
        state='Líquido',
        country='Atlântida'
    )
