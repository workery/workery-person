CREATE TABLE "auths" (
  "id" TEXT PRIMARY KEY,
  "phone" VARCHAR(15) UNIQUE NOT NULL,
  "email" VARCHAR(64) UNIQUE NOT NULL,
  "google" TEXT UNIQUE,
  "apple" TEXT UNIQUE,
  "password" TEXT NOT NULL,
  "create_in" TIMESTAMP NOT NULL,
  "enable_in" TIMESTAMP
);

CREATE TABLE "roles" (
  "id" TEXT PRIMARY KEY,
  "name" TEXT NOT NULL,
  "description" TEXT NOT NULL
);

CREATE TABLE "auth_role" (
  "auth" TEXT NOT NULL,
  "role" TEXT NOT NULL,
  PRIMARY KEY ("auth", "role")
);

CREATE INDEX "idx_auth_role"
  ON "auth_role" ("role");

ALTER TABLE "auth_role"
  ADD CONSTRAINT "fk_auth_role__auth"
  FOREIGN KEY ("auth")
  REFERENCES "auths" ("id");

ALTER TABLE "auth_role"
  ADD CONSTRAINT "fk_auth_role__role"
  FOREIGN KEY ("role")
  REFERENCES "roles" ("id")