from typing import (
    NoReturn,
    Self
)
from uuid import UUID
from workery_person.controllers.persons import (
    Person
)


class TestAddress:

    def test_new_address(
        self: Self,
        person_created: dict,
        default_address: dict,
        address_created: dict
    ) -> NoReturn:
        person = Person(
            identifier=person_created[
                'identifier'
            ]
        )
        assert isinstance(
            person.address.new(
                location=default_address[
                    'location'
                ],
                district=default_address[
                    'district'
                ],
                city=default_address[
                    'city'
                ],
                state=default_address[
                    'state'
                ],
                country=default_address[
                    'country'
                ]
            ),
            str
        )

    def test_update_address(
        self: Self,
        address_created: dict[str],
        default_address: dict,
        person_created: dict
    ) -> NoReturn:
        person = Person(
            identifier=person_created[
                'identifier'
            ]
        )
        assert person.address.update(
            record=address_created[
                'id'
            ],
            values={
                'location': default_address[
                    'location'
                ]
            }
        ) == 1

    def test_show_all_addresses(
        self: Self,
        person_created: dict
    ) -> NoReturn:
        person = Person(
            identifier=person_created[
                'identifier'
            ]
        )
        values = person.address.all
        assert isinstance(values, list) == True
        assert len(values) > 0
