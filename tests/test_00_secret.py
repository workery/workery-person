from typing import (
    NoReturn,
    Self
)
from workery_person.controllers.secrets import (
    Secret
)


class TestSecret:

    text = 'Any text'

    def test_encrypt_bytes(
        self: Self
    ) -> NoReturn:
        assert type(
            Secret.encrypt_text(
                self.text.encode(
                    encoding='utf-8'
                )
            )
        ) == str

    def test_encrypt_string(
        self: Self
    ) -> NoReturn:
        assert type(
            Secret.encrypt_text(
                self.text
            )
        ) == str

    def test_decrypt(
        self: Self
    ) -> NoReturn:
        encrypted_text = Secret.encrypt_text(
            self.text
        )
        assert Secret.decrypt_text(
            encrypted_text
        ) == self.text
