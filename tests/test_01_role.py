from datetime import datetime
from typing import (
    Callable,
    NoReturn,
    Self
)
from uuid import UUID
from workery_person.connectors.sql_queries import (
    SQLQuery
)
from workery_person.controllers.persons import (
    Person
)
from workery_person.controllers.roles import (
    Role
)


class TestRole:
    def test_create_new_role(
        self: Self,
        default_role: dict
    ) -> NoReturn:
        Role.new(
            name=default_role['name'],
            description=default_role['description']
        )
        assert isinstance(
            UUID(
                SQLQuery.show(
                    """
                        SELECT id
                        FROM roles
                        WHERE name = %s
                    """,
                    (
                        default_role['name'],
                    )
                )[0][0]
            ),
            UUID
        ) == True

    def test_rename_role(
        self: Self,
        default_role: dict,
        other_role: dict
    ) -> NoReturn:
        role_target = str(
            SQLQuery.show(
                """
                    SELECT id
                    FROM roles
                    WHERE name = %s
                    LIMIT 1 ;
                """,
                (
                    default_role['name'],
                )
            )[0][0]
        )
        assert Role.rename(
            role_target,
            other_role['name']
        ) == True
        assert SQLQuery.show(
            """
                SELECT name
                FROM roles
                WHERE id = %s
                LIMIT 1 ;
            """,
            (role_target, )
        )[0][0] == other_role['name']

    def test_change_role_description(
        self: Self,
        default_role: dict,
        other_role: dict
    ) -> NoReturn:
        role_target = str(
            SQLQuery.show(
                """
                    SELECT id
                    FROM roles
                    WHERE name = %s
                    LIMIT 1 ;
                """,
                (
                    other_role['name'],
                )
            )[0][0]
        )
        assert Role.change_description(
            role_target,
            other_role['description']
        ) == True
        assert SQLQuery.show(
            """
                SELECT description
                FROM roles
                WHERE id = %s
            """,
            (role_target, )
        )[0][0] == other_role['description']

    def test_add_person_to_role(
        self: Self,
        person_created: dict,
        other_role: dict
    ) -> NoReturn:
        person = Person.auth.login(
            value=person_created['phone'],
            password=person_created['password'].encode()
        )
        for role in Role.show:
            if role['name'] == other_role['name']:
                other_role['id'] = role['id']
        assert Role.add_person(
            person=person,
            role=other_role['id']
        ) == True

    def test_remove_person_to_role(
        self: Self,
        person_created: dict,
        other_role: dict
    ) -> NoReturn:
        person = Person.auth.login(
            value=person_created['phone'],
            password=person_created['password'].encode()
        )
        for role in Role.show:
            if role['name'] == other_role['name']:
                other_role['id'] = role['id']
        assert Role.remove_person(
            person=person,
            role=other_role['id']
        ) == True

    def test_show_roles(
        self: Self
    ) -> NoReturn:
        assert (
            isinstance(
                Role.show, list
            )
        ) == True
        assert (
            isinstance(
                Role.show[0], dict
            )
        ) == True

    def test_removing_role(
        self: Self,
        other_role: dict
    ) -> NoReturn:
        role_target = str(
            SQLQuery.show(
                """
                    SELECT id
                    FROM roles
                    WHERE name = %s
                    LIMIT 1 ;
                """,
                (
                    other_role['name'],
                )
            )[0][0]
        )
        assert Role.remove(
            role_target
        ) == True
        assert len(
            SQLQuery.show(
                """
                    SELECT *
                    FROM roles
                    WHERE id = %s ;
                """,
                (role_target, )
            )
        ) == 0
