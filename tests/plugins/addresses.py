from faker import Faker
from os import getenv
from pytest import fixture
from rethinkdb import RethinkDB
from rethinkdb.errors import (
    ReqlOpFailedError
)
from workery_person.controllers.addresses import (
    Address
)


@fixture(scope='module')
def address_created(
    faker: Faker,
    person_created: dict
) -> dict[str]:
    data = {
        'person': person_created[
            'identifier'
        ],
        'location': 'Location default',
        'district': 'District default',
        'city': 'City default',
        'state': 'State default',
        'country': 'Country default'
    }
    table_name = 'addresses'
    server = RethinkDB()
    connection = server.connect(
        db=getenv(
            'WORKERY_NAME'
        )
    )
    table = server.table(
        table_name
    )
    table.insert(
        data
    ).run(
        connection
    )[
        'generated_keys'
    ]
    return [
        a for a in table.filter(
            data
        ).run(
            connection
        )
    ][0]

@fixture(scope='module')
def default_address(
    faker: Faker
) -> dict[str]:
    return {
        'location': faker.street_address(),
        'district': faker.neighborhood(),
        'city': faker.city(),
        'state': faker.state_abbr(),
        'country': faker.country()
    }
