from faker import Faker
from pytest import fixture
from workery_person.controllers.addresses import (
    Address
)


@fixture(scope='module')
def faker() -> Faker:
    return Faker(
        locale='pt-br'
    )
