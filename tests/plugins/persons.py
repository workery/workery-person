from argon2 import PasswordHasher
from datetime import datetime, UTC
from faker import Faker
from os import getenv
from psycopg import connect
from pytest import fixture
from uuid import uuid4
from workery_person.controllers.persons import (
    Person
)
from ..utils import format_phone


__hash = PasswordHasher()


@fixture(scope='module')
def person_created(
    faker: Faker
) -> dict:
    person = {
        'phone': format_phone(
            faker.cellphone_number()
        ),
        'email': faker.email(),
        'password': faker.password()
    }
    with connect(
        getenv(
            'WORKERY_SQL'
        )
    ) as connection:
        with connection.cursor() as cursor:
            cursor.execute(
                """
                    INSERT INTO auths (
                        id, phone, email, password, create_in
                    )
                    VALUES (
                        %s, %s, %s, %s, %s
                    )
                """,
                (
                    uuid4(),
                    person['phone'],
                    person['email'],
                    __hash.hash(
                        person['password']
                    ),
                    datetime.now(UTC)
                )
            )
            person.update(
                {
                    'identifier': str(
                        cursor.execute(
                            """
                                SELECT id
                                FROM  auths
                                WHERE phone = %s
                            """,
                            (person['phone'], )
                        ).fetchall()[0][0]
                    )
                }
            )
    return person

@fixture(scope='module')
def default_person(
    faker: Faker
) -> dict[str]:
    person = {
        'email': faker.email(),
        'phone': format_phone(
            faker.cellphone_number()
        ),
        'password': faker.password(
            length=20
        ),
        'google': faker.password(
            length=20
        ),
        'apple': faker.password(
            length=20
        )
    }
    Person.auth.new(
        email=person['email'],
        phone=person['phone'],
        password=person['password']
    )
    person.update(
        {
            'id': Person.auth.login(
                value=person['email'],
                password=person['password']
            )
        }
    )
    return person

@fixture(scope='module')
def other_person(
    faker: Faker
) -> dict[str]:
    return {
        'email': faker.email(),
        'phone': format_phone(
            faker.cellphone_number()
        ),
        'password': faker.password(
            length=20
        ),
        'google': faker.password(
            length=20
        ),
        'apple': faker.password(
            length=20
        )
    }

@fixture(scope='function')
def anonymous(
    faker: Faker
) -> dict[str]:
    return {
        'email': faker.email(),
        'phone': format_phone(
            faker.cellphone_number()
        ),
        'password': faker.password(
            length=20
        ),
        'google': faker.password(
            length=20
        ),
        'apple': faker.password(
            length=20
        )
    }
