include .env

cache=${HOME}/Projetos/Workery/cache

clean:
	sudo rm -rRf ${cache}/postgres/*
	sudo rm -rRf ${cache}/redis/*
	sudo rm -rRf ${cache}/rethinkdb/*
debug:
	ipython -i -m workery_person
pg:
	sudo podman run --rm --replace \
		--name ${WORKERY_NAME}-postgres \
		--publish 5432:5432 \
		--env POSTGRES_PASSWORD=${WORKERY_KEY} \
		--env POSTGRES_USER=${WORKERY_NAME} \
		--volume ${cache}/postgres:/var/lib/postgresql \
		--detach postgres:latest
pods:
	make pg
	make rethinkdb
	make redis
	make tables
psql:
	psql ${WORKERY_SQL}
redis:
	sudo podman run --rm --replace\
		--name ${WORKERY_NAME}-redis \
		--publish 6379:6379 \
		--volume ${cache}/redis:/data \
		--detach redis:latest
rethinkdb:
	sudo podman run --rm --replace\
		--name ${WORKERY_NAME}-rethinkdb \
		--publish 28015:28015 \
		--publish 8080:8080 \
		--volume ${cache}/rethinkdb:/data/rethinkdb_data \
		--detach rethinkdb:latest
tables:
	until psql --file postgres_tables.sql ${WORKERY_SQL} 2&> /dev/null ; do sleep 1 ; done
test:
	python -m pytest \
		--cov-report term-missing \
		--cov=workery_person \
		--verbosity=9 \
		--showlocals \
		-s

all:
	make pods ; make tables ; make test
doc:
	cd docs ; mkdocs serve
dump:
	pg_dump \
		${WORKERY_SQL} > postgresql_tables.sql
make allclean:
	make clean ; make all
