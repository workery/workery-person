# Address

To recap, we have to have a person registered:

```python
from workery_person import Person

Person.auth.new(
    phone='+5512345678901',
    email='new_person@email.com',
    password='wR0n6p4$5w0rD4N07h1n6'
)
```

...and we must log in:

```python
me = Person(
    identifier=Person.auth.login(
        value='new_person@email.com',
        password='wR0n6p4$5w0rD4N07h1n6'
    )
)
```

We do not have any registered address. Let's do this:

```python
me.address.all
>>> []
new_address = me.address.new(
    location='Rua das casas, sn',
    district='Beco do caixão',
    city='Piripiri',
    state='Sólido',
    country='Samsara'
)
new_address
>>> '1eeabe6e-b0cc-406f-8145-d6d830c54ba4'
```

This number is the id of the address in the database. It is with this number that we can manipulate the newly created address. Let's say you don't know which addresses you registered. We will then do a search:

```python

me.address.all
>>> [{'city': 'Piripiri',
>>> 'country': 'Samsara',
>>> 'district': 'Beco do caixão',
>>> 'id': '1eeabe6e-b0cc-406f-8145-d6d830c54ba4',
>>> 'location': 'Rua das casas, sn',
>>> 'person': 'aa9e2aa1-ab7a-4ed1-8852-b87989f72de4',
>>> 'state': 'Sólido'}]
```

Note that the value of the new_address variable is the same as that returned by the query:

```python
me.address.all[0]['id']
>>> '1eeabe6e-b0cc-406f-8145-d6d830c54ba4'
```

Suppose we change streets, in another neighborhood:

```python
me.address.update(
    record=me.address.all[0]['id'],
    values={
        'location': 'Estrada dos andarilhos, km 3.5',
        'district': 'Cafundó'
    }
)
>>> 1
```

Let's see if there really has been a change:

```python
me.address.all
>>> [{'city': 'Piripiri',
>>> 'country': 'Samsara',
>>>  'district': 'Cafundó',
>>>  'id': '2847afb6-1747-47a6-9c2c-5728c5e36f51',
>>>  'location': 'Estrada dos andarilhos, km 3.5',
>>>  'person': '094cb285-0def-4bbf-a8b1-4ec160956660',
>>>  'state': 'Sólido'}]
```
