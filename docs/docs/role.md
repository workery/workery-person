# Role

Roles are the access rules for each person.
In order for us to add people to these rules, we need to create them:

```python
from workery_person import Role

Role.new(
    name='keeper',
    description='The owner of the whole thing.'
)
>>> True
```

Let's see if it really worked:

```python
Role.show
>>> [{'id': UUID('79607ef1-a986-410f-9a69-5b1945f01be8'),
>>>  'name': 'keeper',
>>>  'description': 'The owner of the whole thing.'}]
```

You may want to change the rule name:

```python
Role.rename(
    role='79607ef1-a986-410f-9a69-5b1945f01be8',
    new_name='owner'
)
Role.show
>>> [{'id': UUID('79607ef1-a986-410f-9a69-5b1945f01be8'),
>>>  'name': 'owner',
>>>  'description': 'The owner of the whole thing.'}]
```

We can also change its description:

```python
Role.change_description(
    role='79607ef1-a986-410f-9a69-5b1945f01be8',
    new_description='Business owner.'
)
Role.show
>>> [{'id': UUID('79607ef1-a986-410f-9a69-5b1945f01be8'),
>>>  'name': 'owner',
>>>  'description': 'Business owner.'}]
```

Let's now put our user in this rule:

```python
me = Person(
    ...:     identifier=Person.auth.login(
    ...:         value='new_person@email.com',
        password='wR0n6p4$5w0rD4N07h1n6'
    )
)
me.auth.identifier
>>> '06191f29-b2cb-4669-a216-8bb1003209a8'
role = Role.show[0]['id']
role
>>> UUID('79607ef1-a986-410f-9a69-5b1945f01be8')
Role.add_person(
    person=me.auth.identifier,
    role=Role.show[0]['id']
)
>>> True
```

If we were to visualize our data, we might mistakenly imagine that our user is not part of the rule we just entered:

```python
me = Person(
    identifier=Person.auth.login(
        value='new_person@email.com',
        password='wR0n6p4$5w0rD4N07h1n6'
    )
)
me.auth.__dict__
>>> {'identifier': UUID('06191f29-b2cb-4669-a216-8bb1003209a8'),
>>> 'phone': '+5510987654321',
>>> 'email': 'my_new@email.net',
>>> 'google': None,
>>> 'apple': None,
>>> 'create_in': datetime.datetime(2024, 2, 15, 18, 57, 27, 114511),
>>> 'enable_in': None,
>>> 'roles': []}
```

For the changes to take effect, we must log in again:

```python
me = Person(
    identifier=Person.auth.login(
        value='new_person@email.com',
        password='wR0n6p4$5w0rD4N07h1n6'
    )
)
me.auth.__dict__
>>> {'identifier': UUID('06191f29-b2cb-4669-a216-8bb1003209a8'),
>>> 'phone': '+5510987654321',
>>> 'email': 'my_new@email.net',
>>> 'google': None,
>>> 'apple': None,
>>> 'create_in': datetime.datetime(2024, 2, 15, 18, 57, 27, 114511),
>>> 'enable_in': None,
>>> 'roles': ['owner']}
```
