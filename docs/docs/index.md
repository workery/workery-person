# Workery Person
Core of the Workery ecosystem.

## Topics

[Creating a person](person.md)

[Authenticating](auth.md)

[Managing addresses](address.md)

[Managing cookies](key.md)

[Managing rules](role.md)

[Managing secret keys](secret.md)